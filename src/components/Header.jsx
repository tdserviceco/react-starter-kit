import React, { Component } from 'react'
import {Navigation} from './Navigation.jsx'
import {Images} from './Images.jsx'
export class Header extends Component {
   render(){
       return(
           <header>
            <Images />
            <Navigation />
           </header>
       )
   }
}