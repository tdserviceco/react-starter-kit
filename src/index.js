import React, { Component } from 'react'
import { render } from 'react-dom'
import { Header } from './components/Header.jsx'
import { Home } from './components/Home.jsx'
import css from './app.scss'

class App extends Component {
    render() {
        return (
            <div>
            <Header />
                <div className="container">
                    <Home />
                </div> 
            </div>
        )
    }
}

render(<App />, window.document.getElementById('reactJS'))