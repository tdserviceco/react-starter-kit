//Change Title from index.ejs here:
const titleForIndex = 'React Project'

//Required from plugins
const webpack = require('webpack')
const path = require('path')
const glob = require('glob');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const bootstrapEntryPoints = require('./webpack.bootstrap.config')
const publicPathProd =  path.join(path.basename('../'))

//If statments for Dev and Prod
const isProd = process.env.NODE_ENV === 'production' //if production is true or false
const urlProd = 'url-loader?limit=100000&name=/images/[hash].[ext]'
const urlDev = 'url-loader?limit=100000&name=./images/[name].[ext]'
const cssDev = ['style-loader','css-loader','sass-loader']
const cssProd = ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: ['css-loader','sass-loader'],
    publicPath: publicPathProd,
})

//Configurations Setup
const urlLoaderConfig = isProd ? urlProd : urlDev
const cssConfig = isProd ? cssProd : cssDev
const bootstrapConfig = isProd ? bootstrapEntryPoints.prod : bootstrapEntryPoints.dev 

//Configuration goes here
const config = {
    entry: {
        app: './src/index.js',
        bootstrap: bootstrapConfig
    },
    output: {
        path: path.resolve(__dirname + '/dist'),
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
        {
            test: /\.scss$/,
            use: cssConfig
        },
        {
            test: /\.jsx?$/,
            exclude: /(node_modules|bower_components)/,
            use: [
            'babel-loader'
            ]
        },
        {
            test: /\.(png|svg|jpe?g|gif)$/i,
            use: urlLoaderConfig
        },      
        { 
            test: /\.(woff2?|svg)$/, 
            use: 'url-loader?limit=10000' 
        },
        { 
            test: /\.(ttf|eot)$/, 
            use: 'file-loader' 
        },
        {
            test: /\.ico$/,
            use: 'file-loader'
        },
        { 
            test: /bootstrap[\/\\]js[\/\\]src[\/\\]/, 
            use: 'imports?jQuery=jquery' ,
        }
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 3000,
        stats: 'errors-only',
        // open: true,
        hot: true
    },
    plugins: [
    new HtmlWebpackPlugin({
        title: titleForIndex,
        minify: {
            collapseWhitespace: isProd
        },
        hash: true,
        inject: true,
        template: './src/index.ejs', // Load a custom template (ejs by default see the FAQ for details)
        favicon: './favicon.ico'
    }),
    new ExtractTextPlugin({
        filename: 'css/[name].css',
        disable: !isProd,
        allChunks: true
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery",
        Tether: "tether",
        "window.Tether": "tether",
        Alert: "exports-loader?Alert!bootstrap/js/dist/alert",
        Button: "exports-loader?Button!bootstrap/js/dist/button",
        Carousel: "exports-loader?Carousel!bootstrap/js/dist/carousel",
        Collapse: "exports-loader?Collapse!bootstrap/js/dist/collapse",
        Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
        Modal: "exports-loader?Modal!bootstrap/js/dist/modal",
        Popover: "exports-loader?Popover!bootstrap/js/dist/popover",
        Scrollspy: "exports-loader?Scrollspy!bootstrap/js/dist/scrollspy",
        Tab: "exports-loader?Tab!bootstrap/js/dist/tab",
        Tooltip: "exports-loader?Tooltip!bootstrap/js/dist/tooltip",
        Util: "exports-loader?Util!bootstrap/js/dist/util",
    }),
    ]}
    

    //Dont touch this
    module.exports = config